#include "imgkernel.h"
#include <QImage>

std::map<QString, ImgKernel>ImgKernel::kernels{
    {"identity", ImgKernel(0,0,0,
                           0,1,0,
                           0,0,0)},
    {"blur", ImgKernel(0.0625, 0.125, 0.0625,
                       0.125, 0.25, 0.125,
                       0.0625, 0.125, 0.0625)},
    {"sharpen", ImgKernel(0, -1, 0,
                          -1, 5, -1,
                          0, -1, 0)},
    {"emboss", ImgKernel(-2, -1, 0,
                          -1, 1, 1,
                          0, 1, 2)},
    {"bottom sobel", ImgKernel(-1, -2, -1,
                               0, 0, 0,
                               1, 2, 1)},
    {"leftSobel", ImgKernel(1, 0, -1,
                            2, 0, -2,
                            1, 0, -1)},
    {"outline", ImgKernel(-1, -1, -1,
                          -1, 8, -1,
                          -1, -1, -1)}
};

ImgKernel::ImgKernel(float m11, float m12, float m13,
                     float m21, float m22, float m23,
                     float m31, float m32, float m33){
    matrix = new float[3][3]{{m11, m12, m13}, {m21, m22, m23}, {m31, m32, m33}};
}

static bool equals(float m1[3][3], float m2[3][3]){
    for(int i = 0; i < 3; i++){
        for(int j = 0; j < 3; j++){
            if(int(m1[i][j] + 0.005f) != int(m2[i][j] + 0.005f)){
                return false;
            }
        }
    }
    return true;
}

QImage &ImgKernel::apply(QImage &img){
    if(equals(matrix, kernels.at("identity").matrix)){
        return img;
    }
    QImage &nimg = *new QImage(img);
    for(int i = 1; i < img.height() - 1; i++){
        for(int j = 1; j < img.width() - 1; j++){
            float fr = 0, fg = 0, fb = 0;
            for(int r = 0; r < 3; r++){
                for(int s = 0; s < 3; s++){
                    QColor pcol(img.pixel(j - 1 + s, i - 1 + r));
                    fr += pcol.red() * matrix[r][s];
                    fg += pcol.green() * matrix[r][s];
                    fb += pcol.blue() * matrix[r][s];
                }
            }

#define tocol(c, fp){\
            c = int(fp + 0.05f);\
            if(c < 0) c = 0;\
            else if(c > 255) c = 255;}

            int r, g, b;
            tocol(r, fr); tocol(g, fg); tocol(b, fb);

            nimg.setPixelColor(j, i, QColor(r, g, b));
        }
    }
    return nimg;
}
