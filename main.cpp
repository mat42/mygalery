#include "mainwindow.h"
#include <QApplication>
#include <QString>
#include <QFileInfo>
#include <iostream>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    if(argc > 1){
        QString path(argv[1]);
        QFileInfo fileInfo(path);
        if(!fileInfo.exists()){
            std::cout << "Error: No such file or directory" << std::endl;
        }else if(!fileInfo.isFile()){
            std::cout << "Error: Not a file" << std::endl;
        }else if(!w.loadImage(path)){
            std::cout << "Error while loading the image" << std::endl;
        }
    }
    return a.exec();
}
