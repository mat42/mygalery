#include "flowlayout.h"
#include <QWidget>

FlowLayout::FlowLayout(int margin, int hSpacing, int vSpacing)
    : hSpace(hSpacing), vSpace(vSpacing){
    setContentsMargins(margin, margin, margin, margin);
}

FlowLayout::~FlowLayout(){
    clear();
}

void FlowLayout::addItem(QLayoutItem *item){
    itemList.append(item);
}

int FlowLayout::count() const{
    return itemList.size();
}

QLayoutItem *FlowLayout::itemAt(int index) const{
    return itemList.value(index);
}

QLayoutItem *FlowLayout::takeAt(int index){
    if (index >= 0 && index < itemList.size())
        return itemList.takeAt(index);
    return nullptr;
}

void FlowLayout::clear(){
    QLayoutItem *item;
    while ((item = takeAt(0))){
        delete item->widget();
        delete item;
    }
}

Qt::Orientations FlowLayout::expandingDirections() const{
    return nullptr;
}

bool FlowLayout::hasHeightForWidth() const{
    return true;
}

int FlowLayout::heightForWidth(int width) const{
    int height = doLayout(QRect(0, 0, width, 0), true);
    return height;
}

void FlowLayout::setGeometry(const QRect &rect){
    QLayout::setGeometry(rect);
    doLayout(rect, false);
}

QSize FlowLayout::minimumSize() const{
    QSize size;
    for(auto item : itemList){
        size = size.expandedTo(item->sizeHint());
    }
    const QMargins margins = contentsMargins();
    size += QSize(margins.left() + margins.right(), margins.top() + margins.bottom());
    return size;
}

QSize FlowLayout::sizeHint() const{
    return minimumSize();
}

int FlowLayout::doLayout(const QRect &rect, bool testOnly) const{
    int left, top, right, bottom;
    getContentsMargins(&left, &top, &right, &bottom);

    int x = rect.x(), y = rect.y();
    int lineHeight = 0;
    for(auto item : itemList){
        int nextX = x + item->sizeHint().width() + hSpace;
        if (nextX - hSpace > rect.right() && lineHeight > 0) {
            x = rect.x();
            y = y + lineHeight + vSpace;
            nextX = x + item->sizeHint().width() + hSpace;
            lineHeight = 0;
        }
        if (!testOnly){
            item->setGeometry(QRect(QPoint(x, y), item->sizeHint()));
        }

        x = nextX;
        lineHeight = qMax(lineHeight, item->sizeHint().height());
    }
    return lineHeight + y - rect.y() + bottom;
}
