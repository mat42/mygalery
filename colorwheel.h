#ifndef COLORWHEEL_H
#define COLORWHEEL_H
#include <QLabel>
class QColor;
class QImage;

class ColorWheel: public QLabel
{
public:
    ColorWheel(QColor curColor, int w, int h);
    void setColor(QColor color);
private:
    QImage *image;
    QColor curColor;
    void drawPalette(QColor color, int *colorX, int *colorY);
    void resizeEvent(QResizeEvent *)override;
};

#endif // COLORWHEEL_H
