#ifndef IMGFILTER_H
#define IMGFILTER_H
class QImage;

struct ImgFilter
{
    int dh;
    int ds;
    int dv;
    ImgFilter(int h, int s, int v);
    ImgFilter();
    QImage &apply(QImage &);
};

#endif // IMGFILTER_H
