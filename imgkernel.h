#ifndef IMGKERNEL_H
#define IMGKERNEL_H
#include <map>

class QImage;
class QMatrix;
class QString;

class ImgKernel
{
public:
    ImgKernel(float m11, float m12, float m13,
              float m21, float m22, float m23,
              float m31, float m32, float m33);

    static std::map<QString, ImgKernel> kernels;
    QImage &apply(QImage&);
private:
    float (*matrix)[3];
};

#endif // IMGKERNEL_H
