#include "editimagebox.h"
#include <QPainter>
#include <QMouseEvent>
#include <cmath>

EditImageBox::EditImageBox(){
    _editMode = EditMode::None;
    pen = QPen(QBrush(QColor(255, 0, 0, 255)), 5, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
    drawingPoints = new std::list<DrawingPoint>();
}

EditImageBox::~EditImageBox(){
    delete drawingPoints;
}

void EditImageBox::setPenColor(QColor c){
    pen.setColor(c);
}

QColor EditImageBox::penColor(){
    return pen.color();
}

void EditImageBox::setPenWidth(int t){
    pen.setWidth(t);
}

int EditImageBox::penWidth(){
    return pen.width();
}

void EditImageBox::setEditMode(EditMode value){
    _editMode = value;
    setPixmap(originalPixmap->scaled(pixmap()->size()));
    if(editMode() == EditMode::Paint){
        setPixmap(drawPaintings(*pixmap()));
    }
    if(editMode() == EditMode::Crop){
        int x = int(0.1 * pixmap()->width());
        int y = int(0.1 * pixmap()->height());
        drawCropFrame(x, y, pixmap()->width() - x, pixmap()->height() - y);
        updateFrameRelSize();
    }
}

void EditImageBox::reset(){
    clearPaintings();
    setEditMode(_editMode);
}

static bool inRectangle(int x, int y, QRect &rect){
    x -= rect.x();
    y -= rect.y();
    return x >= 0 && x <= rect.width() && y >= 0 && y <= rect.height();
}

void EditImageBox::drawCropFrame(QRect rect, int mouse_x, int mouse_y){
    drawCropFrame(rect.x(), rect.y(), rect.width() + rect.x(), rect.height() + rect.y(), mouse_x, mouse_y);
}

static int lastRectIdx = -1;

static int activeRect_i = -1;
static int activeRect_j = -1;

void EditImageBox::drawCropFrame(int x1, int y1, int x2, int y2, int mouse_x, int mouse_y){

    //to move evenly on the edges without resizing
    if(!inImgX(x1) || !inImgX(x2)){
        x1 = cropFrameRect.x();
        x2 = cropFrameRect.x() + cropFrameRect.width();
    }
    if(!inImgY(y1) || !inImgY(y2)){
        y1 = cropFrameRect.y();
        y2 = cropFrameRect.y() + cropFrameRect.height();
    }

    int w = x2 - x1;
    int h = y2 - y1;

    if(w < 50){
        w = 50;
        x2 = x1 + 50;
    }
    if(h < 50){
        h = 50;
        y2 = y1 + 50;
    }

    cropFrameRect = QRect(x1, y1, w, h);

    int fld = int(0.05 * (std::sqrt(w * w + h * h)));
    if(fld < 10){
        fld = 10;
    }
    if(fld > 20){
        fld = 20;
    }

    int right_x = x2 - fld;
    int bottom_y = y2 - fld;

    int centW = w - 2 * fld;
    int centH = h - 2 * fld;

    QRect rects[] = {
        cropFrame[0][0] = QRect (x1, y1, fld, fld),
        cropFrame[0][1] = QRect(x1 + fld, y1, centW, fld),
        cropFrame[0][2] = QRect (right_x, y1, fld, fld),

        cropFrame[1][0] = QRect(right_x, y1 + fld, fld, centH),
        cropFrame[1][1] = QRect(x1 + fld, y1 + fld, centW, centH),
        cropFrame[1][2] = QRect(x1, y1 + fld, fld, centH),

        cropFrame[2][0] = QRect (x1, bottom_y, fld, fld),
        cropFrame[2][1] = QRect(x1 + fld, bottom_y, centW, fld),
        cropFrame[2][2] = QRect (right_x, bottom_y, fld, fld),
    };


    QPixmap pm = originalPixmap->scaled(pixmap()->size());
    QPainter painter(&pm);
    painter.setPen(cropPen);

    if(activeRect_i == -1 && activeRect_j == -1){
        bool otherSelected = false;

        for(int i = 0; i < 9; i++){
            if(inRectangle(mouse_x, mouse_y, rects[i])){
                otherSelected = true;
                painter.fillRect(rects[i], fillBrush);
                lastRectIdx = i;
                break;
            }
        }

        if(!otherSelected){
            lastRectIdx = -1;
        }
    }else{
        painter.fillRect(cropFrame[activeRect_i][activeRect_j], fillBrush);
    }

    painter.drawRects(rects, 9);

    painter.setPen(QPen(QBrush(QColor(0, 0, 0)), 1));
    painter.drawRect(cropFrameRect.x() - cropPen.width() / 2, cropFrameRect.y() - cropPen.width() / 2,
                     cropFrameRect.width() + cropPen.width(), cropFrameRect.height() + cropPen.width());
    for(QRect rect : rects){
        painter.drawRect(rect.x() + cropPen.width() / 2,
                         rect.y() + cropPen.width() / 2,
                         rect.width() - cropPen.width() - 1,
                         rect.height() - cropPen.width() - 1);
    }
    setPixmap(pm);
}

EditMode EditImageBox::editMode(){
    return _editMode;
}

static int last_x = -1, last_y = -1;

int EditImageBox::imgX(int x){
    return x - int((width() - pixmap()->width()) / 2.0 + 0.5);
}

int EditImageBox::imgY(int y){
    return y -  int((height() - pixmap()->height()) / 2.0 + 0.5);
}

bool EditImageBox::inImgX(int x){
    return x >= 0 && x <= pixmap()->width();
}

bool EditImageBox::inImgY(int y){
    return y >= 0 && y <= pixmap()->height();
}

void EditImageBox::mousePressEvent(QMouseEvent *e){
    if(!inImgX(imgX(e->x())) || !inImgY(imgY(e->y()))){
        return;
    }

    last_x = imgX(e->x());
    last_y = imgY(e->y());

    if(editMode() == EditMode::Crop){
        onCropFrameEntered(imgX(e->x()), imgY(e->y()));
    }

    if(editMode() == EditMode::Paint){
        addDrawingPoint(last_x, last_y);
    }
}

void EditImageBox::mouseMoveEvent(QMouseEvent *e){
    if(!inImgX(imgX(e->x())) || !inImgY(imgY(e->y()))){
        return;
    }

    if(editMode() == EditMode::Crop){
        int x = imgX(e->x());
        int y = imgY(e->y());
        onCropFrameEntered(x, y);
        dragOnCropFrame(x, y);
    }

    if(editMode() == EditMode::Paint){
        QPixmap pm  = *pixmap();
        QPainter painter(&pm);
        painter.setPen(pen);
        painter.drawLine(imgX(e->x()), imgY(e->y()), last_x, last_y);
        last_x = imgX(e->x());
        last_y = imgY(e->y());
        addDrawingPoint(last_x, last_y);
        setPixmap(pm);
    }
}

void EditImageBox::onCropFrameEntered(int x, int y){
    if(inRectangle(x, y, cropFrameRect) || lastRectIdx != -1){
        drawCropFrame(cropFrameRect, x, y);
    }
}

void EditImageBox::dragOnCropFrame(int x, int y){

    int dx = x - last_x;
    int dy = y - last_y;
    QRect &cfr = cropFrameRect;

    QRect rects[3][3] = {
        {QRect(cfr.x() + dx, cfr.y() + dy, cfr.width() - dx, cfr.height() - dy),

         QRect(cfr.x(), cfr.y() + dy, cfr.width(), cfr.height() - dy),

         QRect(cfr.x(), cfr.y() + dy, cfr.width() + dx, cfr.height() - dy)
        },

        {QRect(cfr.x(), cfr.y(), cfr.width() + dx, cfr.height()),

         QRect(cfr.x() + dx, cfr.y() + dy, cfr.width(), cfr.height()),

         QRect(cfr.x() + dx, cfr.y(), cfr.width() - dx, cfr.height())
        },

        {QRect(cfr.x() + dx, cfr.y(), cfr.width() - dx, cfr.height() + dy),

         QRect(cfr.x(), cfr.y(), cfr.width(), cfr.height() + dy),

         QRect(cfr.x(), cfr.y(), cfr.width() + dx, cfr.height() + dy)
        }
    };

    if(activeRect_i != -1 && activeRect_j != -1){
        drawCropFrame(rects[activeRect_i][activeRect_j]);
    }else{
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                if(inRectangle(x, y, cropFrame[i][j])){
                    drawCropFrame(rects[i][j], x, y);
                    activeRect_i = i;
                    activeRect_j = j;
                    break;
                }
            }
        }
    }

    last_x = x;
    last_y = y;
    updateFrameRelSize();

    //TODO: keep aspect ratio
}

void EditImageBox::mouseReleaseEvent(QMouseEvent *){
    last_x = last_y = -1;
    activeRect_i = activeRect_j = -1;
    drawingPoints->push_back(DrawingPoint::breakPoint());
}

void EditImageBox::addDrawingPoint(int x, int y){
    float xr = x / float(pixmap()->width());
    float yr = y / float(pixmap()->height());
    drawingPoints->push_back(DrawingPoint(xr, yr, pen));
}

void EditImageBox::clearPaintings(){
    drawingPoints->clear();
    setPixmap(originalPixmap->scaled(pixmap()->size()));
}

static int s_absoluteX(float x, const QPixmap *pm){
    return int(x * float(pm->width()) + 0.005f);
}

static int s_absoluteY(float y, const QPixmap *pm){
    return int(y * float(pm->height()) + 0.005f);
}

int EditImageBox::absoluteX(float x){
    return s_absoluteX(x, pixmap());
}

int EditImageBox::absoluteY(float y){
    return s_absoluteY(y, pixmap());
}

void EditImageBox::resizePixmap(QSize size){
    ImageBox::resizePixmap(size);
    if(editMode() == EditMode::Paint){
        setPixmap(drawPaintings(*pixmap()));
    }else if(editMode() == EditMode::Crop){
        updateCropFrame();
    }
}

QPixmap EditImageBox::drawPaintings(QPixmap pm, float penScale){
    if(drawingPoints->size() < 2){
        return pm;
    }
    QPainter painter(&pm);
    DrawingPoint prev = drawingPoints->front();
    for(auto it = ++drawingPoints->begin(); it != drawingPoints->end(); it++){
        QPen pen = it->pen();
        if(penScale > 0){
            pen.setWidth(int(penScale * pen.width() + 0.005f));
        }
        painter.setPen(pen);
        if(it->isBreak() || prev.isBreak()){
            prev = *it;
            continue;
        }
        int x1 = s_absoluteX(prev.x(), &pm);
        int y1 = s_absoluteY(prev.y(), &pm);
        int x2 = s_absoluteX(it->x(), &pm);
        int y2 = s_absoluteY(it->y(), &pm);
        painter.drawLine(x1, y1, x2, y2);
        prev = *it;
    }
    return pm;
}

void EditImageBox::updateCropFrame(){
    drawCropFrame(absoluteX(relFrameStart->x()),
                  absoluteY(relFrameStart->y()),
                  absoluteX(relFrameEnd->x()),
                  absoluteY(relFrameEnd->y()));
}

void EditImageBox::updateFrameRelSize(){
    float fx1 = cropFrameRect.x() / float(pixmap()->width());
    float fy1 = cropFrameRect.y() / float(pixmap()->height());
    float fx2 = (cropFrameRect.x() + cropFrameRect.width()) / float(pixmap()->width());
    float fy2 = (cropFrameRect.y() + cropFrameRect.height()) / float(pixmap()->height());
    relFrameStart = new DrawingPoint(fx1, fy1);
    relFrameEnd = new DrawingPoint(fx2, fy2);
}

QPixmap *EditImageBox::apply(){
    if(editMode() == EditMode::Crop){
        int x = s_absoluteX(relFrameStart->x(), originalPixmap);
        int y = s_absoluteY(relFrameStart->y(), originalPixmap);
        return new QPixmap(originalPixmap->copy(
                               x, y,
                               s_absoluteX(relFrameEnd->x(), originalPixmap) - x,
                               s_absoluteY(relFrameEnd->y(), originalPixmap) - y
                               ));
    }
    if(editMode() == EditMode::Paint){
        return new QPixmap(drawPaintings(*originalPixmap, originalPixmap->height() / float(pixmap()->height())));
    }
    return originalPixmap;
}
