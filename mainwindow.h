#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "editimagebox.h"
#include "flowlayout.h"
#include "imgfilter.h"
#include "colorwheel.h"

class QScrollArea;
class QPushButton;
class QSlider;
class QComboBox;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    bool loadImage(QString fileName);
    QLabel *infoLabel;
    QLabel *pageLabel;
private slots:
    //File
    void open();
    void save();
    void saveAs();
    //View
    void zoomIn();
    void zoomOut();
    void fitImageToWindow();
    void toOriginalSize();
    void reload();
    void showToolbar();
    //Go
    void toNext();
    void toPrevious();
    void toRandom();
    //Edit
    void clockwise();
    void ctrclockwise();
    void flipH();
    void flipV();
    void deleteImage();
    void copy();
    void openEditor();
    //Editor
    void closeEditor();
    void crop();
    void paint();
    void filter();
    void editOK();
    void editCancel();
    void setH(int);
    void setS(int);
    void setV(int);
    void setPenWidth(int);
    void setKernelImage(QString name);
private:
    QMenu *fileMenu, *viewMenu, *goMenu, *editMenu;

    QAction
    //File
    *fileAction,
    *saveAction, *saveAsAction,
    //View
    *zoomInAction,
    *zoomOutAction,
    *originalSizeAction,
    *fitToWindowAction,
    *reloadAction,
    *showToolbarAction,
    //Go
    *nextAction,
    *prevAction,
    *randomAction,
    //Edit
    *clockwiseAction, *ctrclockwiseAction,
    *flipH_Action, *flipV_Action,
    *deleteAction, *copyAcion, *openEditorAction;

    QToolBar *toolbar;

    EditImageBox *imageBox;
    QScrollArea *imageScroll;

    bool resizeMode;
    const double scaleSeq[8] = {0.02, 0.05, 0.10, 0.20, 0.33, 0.50, 0.67, 1};

    QString imagePath;

    QStringList galeryFiles;
    QList<QPushButton *> thumbs;
    int imageIndex;

    FlowLayout *galeryLayout;

    QImage *originalImg, *image;

    QWidget *editor;
    QPushButton *paintButton, *cropButton, *filterButton;
    QWidget *editorDialogPane;
    QWidget *editorHSVwidget;
    QWidget *editorPenWidthWidget;
    QSlider *hSlider, *sSlider, *vSlider, *penWidthSlider;
    QLabel *hLabel, *sLabel, *vLabel;
    ColorWheel *colorWheel;

    ImgFilter curFilter;

    void initToolbar(QVBoxLayout *mainLayout);
    void initWorkspace(QVBoxLayout *mainLayout);
    void initBottomPane(QVBoxLayout *mainLayout);
    void initEditor();
    QHBoxLayout *makeHSVslider(QSlider **slider, QLabel **label, QString name, void (MainWindow::*slot)(int));
    QComboBox *kernelCombo;

    void setImage(const QImage &newImage);

    void initGeometry();

    void createMenu();
    QAction *createAction(QString name, QMenu *menu, QKeySequence keySeq, void (MainWindow::*slot)(), QString icon="", bool enabled = false);

    void toResizeMode();

    double getNextScale(bool inc);

    void loadImage(int index);
    QString explorerDir();
    void loadDir(QString filePath);
    void setupGalery(QString directory);
    void moveSelection(int index, int from = -1);
    void updateInfoLabel();
    void closeEditTool();
    void resetFilter();
    void setHSVsliders(int h, int s, int v);
    void setFilterImage(int h, int s, int v);
    void setPenColor(int h, int s, int v);
    void setColorWheel(int h, int s, int v);
};

#endif // MAINWINDOW_H
