#ifndef PICTUREBOX_H
#define PICTUREBOX_H
#include <QLabel>
class QPixmap;

class ImageBox: public QLabel
{
public:
    ImageBox();
    ~ImageBox() override;
    bool loadImage(QString fileName);
    bool loadImage(const QImage &image);
    QSize originalSize();
    void fitToImage();
    void zoom(double prc);
    double curScalePercent();
protected:
    void resizeEvent(QResizeEvent *event) override;
    QPixmap *originalPixmap;
    virtual void resizePixmap(QSize);
};

#endif // PICTUREBOX_H
