#include "colorwheel.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include<QColor>
#include<QPainter>

#define SELECTOR_R 5


ColorWheel::ColorWheel(QColor color, int w, int h){
    resize(w, h);
    setFixedSize(w, h);
    image = new QImage(w, h, QImage::Format_RGBA64);
    setColor(color);
}

void ColorWheel::resizeEvent(QResizeEvent *e){
    QLabel::resizeEvent(e);
    //setColor(curColor);
}

void ColorWheel::setColor(QColor color)
{
    curColor = color;
    int colorX = width() / 2, colorY = width() / 2;
    drawPalette(color, &colorX, &colorY);

    for (int i = colorY - SELECTOR_R; i < colorY + SELECTOR_R; i++){
        for (int j = colorX - SELECTOR_R; j < colorX + SELECTOR_R; j++)
        {
            int dy = i - colorY;
            int dx = j - colorX;
            double dist = round(sqrt(dx * dx + dy * dy));
            if (dist < SELECTOR_R && dist > (SELECTOR_R - 2))
            {
                image->setPixelColor(j, i, QColor(0, 0, 0));
            }
        }
    }
    setPixmap(QPixmap::fromImage(*image));
}

static inline bool isAround(int a, int b){
    return a >= b - 5 && a <= b + 5;
}

void ColorWheel::drawPalette(QColor color, int *colorX, int *colorY)
{
    int centerX = width() / 2, centerY = height() / 2;
    int r = width() / 2 - 10;
    for (int i = 0; i < width(); i++)
    {
        for (int j = 0; j < width(); j++)
        {
            int dy = i - centerY;
            int dx = j - centerX;

            int dist = int(sqrt(dx * dx + dy * dy) + 0.5);
            if (dist == r + 1)
            {
                image->setPixelColor(j, i, QColor(0, 0, 0));
                continue;
            }
            else if (dist <= r)
            {
                double h = acos(dx / sqrt(dx * dx + dy * dy)) / (2 * M_PI);
                if(dy > 0){
                    h = 1 - h;
                }

                QColor ncol = QColor::fromHsvF(h,
                                              dist / double(r),
                                              color.valueF());

                if(isAround(color.hue(), ncol.hue())
                        && isAround(color.saturation(), ncol.saturation())
                        && isAround(color.value(), ncol.value()))
                {
                    *colorX = j;
                    *colorY = i;
                }
                image->setPixelColor(j, i, ncol);
            }
            else{
                image ->setPixelColor(j, i, QColor(0, 0, 0, 0));
            }
        }
    }
}
