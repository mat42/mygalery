#include "utils.h"
#include <QString>
#include <QStringList>
#include <QDir>
#include <QImageReader>

QString utils::getFileName(QString path){
    return path.split(QDir::separator()).last();
}

QString utils::getDirectoryName(QString path){
    QStringList dirPathList = path.split(QDir::separator());
    dirPathList.removeLast();
    return dirPathList.join(QDir::separator());
}

QString utils::getExtention(QString path){
    QStringList spl = utils::getFileName(path).split(".");
    if(spl.length() == 1){
        return "";
    }
    return spl.last();
}

QStringList utils::supportedImageExtentions(QString prefix){
    QStringList &slist = *new QStringList;
    for(QByteArray byteArr: QImageReader::supportedImageFormats()){
        slist << prefix + QString(byteArr);
    }
    return slist;
}
