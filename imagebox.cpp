#include "imagebox.h"
#include <QResizeEvent>
#include <QApplication>
#include <QDesktopWidget>
#include "mainwindow.h"

QSize ImageBox::originalSize(){
    return originalPixmap == nullptr
            ? *new QSize(0,0)
            : originalPixmap->size();
}

ImageBox::ImageBox(){
    setScaledContents(false);
    setAlignment(Qt::AlignCenter);
    originalPixmap = nullptr;
    setMargin(-100);      //magic tric, allows to downsize the image smoothly
                        //** say -1 does not work that well
}

ImageBox::~ImageBox(){
    if(originalPixmap != nullptr){
        delete originalPixmap;
    }
}

void ImageBox::resizeEvent(QResizeEvent *event){
    if(event->size().height() <= originalSize().height()
            || event->size().width() <= originalSize().width()){
        resizePixmap(event->size());
    }
    QWidget::resizeEvent(event);
}

void ImageBox::fitToImage(){
    resize(pixmap()->size());
}

bool ImageBox::loadImage(QString fileName){
    if(originalPixmap != nullptr){
        delete originalPixmap;
    }
    originalPixmap = new QPixmap;
    bool loaded = originalPixmap->load(fileName);
    setPixmap(*originalPixmap);
    return loaded;
}

bool ImageBox::loadImage(const QImage &image){
    if(originalPixmap != nullptr){
        delete originalPixmap;
    }
    originalPixmap = new QPixmap(QPixmap::fromImage(image));
    setPixmap(*originalPixmap);
    //in case bigger image is loaded
    resize(size().width(), size().height() + 1);
    return true;
}

void ImageBox::resizePixmap(QSize size){
    if(originalPixmap != nullptr){
        setPixmap(originalPixmap->scaled(size, Qt::KeepAspectRatio, Qt::SmoothTransformation));
    }
}

void ImageBox::zoom(double prc){
    resizePixmap(QSize(int(prc * originalPixmap->width() + 0.5),
                       int(prc * originalPixmap->height() + 0.5)));
    fitToImage();
}

double ImageBox::curScalePercent(){
    return int(pixmap()->width() / double(originalSize().width()) * 100 + 0.5) * 0.01;
}
