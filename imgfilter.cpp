#include "imgfilter.h"
#include <QImage>
#include <QColor>

ImgFilter::ImgFilter(int h, int s, int v): dh(h), ds(s), dv(v){}

ImgFilter::ImgFilter(): dh(0), ds(0), dv(0){}

QImage &ImgFilter::apply(QImage &img){
    QImage &nimg = *new QImage(img);
    for(int i = 0; i < img.height(); i++){
        for(int j = 0; j < img.width(); j++){
            QColor color(img.pixel(j, i));
            int h, s, v;
            color.getHsv(&h, &s, &v);
            h += dh; s += ds; v += dv;
    #define fit(a, from, to){\
                if(a < from) a = from;\
                if(a > to) a = to;\
            }
            fit(h, 0, 359); fit(s, 0, 255); fit(v, 0, 255);
            color.setHsv(h, s, v);
            nimg.setPixelColor(j, i, color);
        }
    }
    return nimg;
}
