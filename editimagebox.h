#ifndef EDITIMAGEBOX_H
#define EDITIMAGEBOX_H
#include "imagebox.h"
#include <list>
#include <vector>
#include <QPen>

struct DrawingPoint{
    DrawingPoint(float x, float y, QPen pen = QPen()):_x(x), _y(y), _pen(pen){}
    static DrawingPoint breakPoint(){
        return DrawingPoint(-1, -1);
    }
    float _x;
    float _y;
    QPen _pen;
    float x(){return _x;}
    float y(){return _y;}
    QPen pen(){return _pen;}
    bool isBreak(){
        return x() < 0 || y() < 0;
    }
};

enum EditMode{None, Paint, Crop, Filter};

#define MAX_PEN_WIDTH 50

class EditImageBox: public ImageBox
{
public:
    EditImageBox();
    ~EditImageBox() override;
    void setEditMode(EditMode);
    EditMode editMode();
    void clearPaintings();
    void reset();
    QPixmap *apply();

    QColor penColor();
    int penWidth();
    void setPenColor(QColor);
    void setPenWidth(int);
protected:
    void mouseMoveEvent(QMouseEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void resizePixmap(QSize) override;
private:
    EditMode _editMode;
    QPen pen;
    std::list<DrawingPoint> *drawingPoints;
    QRect cropFrame[3][3];
    QRect cropFrameRect;
    DrawingPoint *relFrameEnd;
    DrawingPoint *relFrameStart;
    const QBrush fillBrush = QBrush(QColor(255, 0, 0, 100));
    const QPen cropPen = QPen(QBrush(QColor(255, 0, 96)), 4);

    int imgX(int x);
    int imgY(int y);
    int absoluteX(float x);
    int absoluteY(float y);
    bool inImgX(int x);
    bool inImgY(int y);
    void addDrawingPoint(int x, int y);
    QPixmap drawPaintings(QPixmap pixmap, float penScale = -1);

    void onCropFrameEntered(int x, int y);
    void drawCropFrame(int x1, int y1, int x2,int y2, int mouse_x = -1, int mouse_y = -1);
    void drawCropFrame(QRect rect, int mouseX = -1, int mouseY = -1);
    void dragOnCropFrame(int x, int y);
    void updateCropFrame();
    void updateFrameRelSize();
};

#endif // EDITIMAGEBOX_H
