#include "mainwindow.h"
#include <QtWidgets>
#include <QDebug>
#include <cstdlib>
#include "imgkernel.h"
#include "utils.h"
#include "colorwheel.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    initGeometry();
    createMenu();

    originalImg = image = nullptr;

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->setMargin(0);

    QWidget *w = new QWidget;
    setCentralWidget(w);
    w->setStyleSheet("background: #606060; color:ffffff; font: bold");
    w->setLayout(mainLayout);

    initToolbar(mainLayout);
    initWorkspace(mainLayout);
    initBottomPane(mainLayout);
}

void MainWindow::initToolbar(QVBoxLayout *mainLayout){
    toolbar = new QToolBar;
    toolbar->setStyleSheet("background:#202020; spacing:10px");
    toolbar->setFixedHeight(30);
    mainLayout->addWidget(toolbar);
    toolbar->setToolButtonStyle(Qt::ToolButtonIconOnly);
    toolbar->addActions(*new QList<QAction *>{
                            prevAction, nextAction});
    toolbar->addSeparator();
    toolbar->addActions(*new QList<QAction *>{
                            zoomInAction, zoomOutAction, fitToWindowAction, originalSizeAction});
    toolbar->addSeparator();
    toolbar->addActions(*new QList<QAction *>{
                            ctrclockwiseAction, clockwiseAction});
}

void MainWindow::initWorkspace(QVBoxLayout *mainLayout){

    QSplitter *splitter = new QSplitter(Qt::Vertical);
    mainLayout->addWidget(splitter);

    imageBox = new EditImageBox();
    imageBox->setStyleSheet("QLabel { background-color : black; }");
    imageScroll = new QScrollArea;
    imageScroll->setStyleSheet("QScrollArea { background-color : black; }");
    imageScroll->setWidget(imageBox);
    imageScroll->setAlignment(Qt::AlignCenter);

    QSplitter *imageSplitter = new QSplitter;
    initEditor();
    imageSplitter->addWidget(editor);
    imageSplitter->addWidget(imageScroll);
    splitter->addWidget(imageSplitter);
    imageSplitter->setSizes(QList<int>({5, 350}));

    QScrollArea *galeryScroll = new QScrollArea;
    splitter->addWidget(galeryScroll);
    QWidget *galery = new QWidget;
    galeryScroll->setWidget(galery);
    galeryScroll->setWidgetResizable(true);
    galeryLayout = new FlowLayout(5, 5, 5);
    galery->setLayout(galeryLayout);

    splitter->setSizes(QList<int>({200, 5}));
}

static QWidget *getVSpacer(){
    QWidget *vSpacer = new QWidget;
    vSpacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    return vSpacer;
}

void MainWindow::initEditor(){

    editor = new QWidget;
    editor->setStyleSheet("*{border:2px solid #af0060; font:bold}"
                          "QPushButton{background: #808080}"
                          "QPushButton:hover{background: #909090}"
                          "QPushButton:disabled{border:4px solid #af0060}"
                          "QSlider{border:0px; background:#606060}"
                          "QSlider:sub-page:horisontal{background: #ff0070}"
                          "QSlider:add-page:horisontal{background: black}"
                          "QSlider:handle:horisontal{background:#ff00a0; border: 1px solid black; margin: -5px 0; width: 10}"
                          "QLabel{border:0px}");
    QVBoxLayout *mainLayout = new QVBoxLayout;
    editor->setLayout(mainLayout);

    QHBoxLayout *headLayout = new QHBoxLayout;
    QPushButton *hideButton = new QPushButton;
    hideButton->setStyleSheet("color: #af0060; font-size: 16px");
    hideButton->setText("X");
    hideButton->setFixedSize(13, 15);
    connect(hideButton, &QPushButton::clicked, this, &MainWindow::closeEditor);
    headLayout->addWidget(getVSpacer());
    headLayout->addWidget(hideButton);
    mainLayout->addLayout(headLayout);

    kernelCombo = new QComboBox;
    for(auto k : ImgKernel::kernels){
        kernelCombo->addItem(k.first);
    }
    kernelCombo->setCurrentText("identity");
    connect(kernelCombo, &QComboBox::currentTextChanged, this, &MainWindow::setKernelImage);
    mainLayout->addWidget(kernelCombo);

    QHBoxLayout *toolsLayout = new QHBoxLayout;

    paintButton = new QPushButton;
    paintButton->setFixedSize(30, 30);
    paintButton->setIcon(QIcon(":/icons/paint"));
    connect(paintButton, &QPushButton::clicked, this, &MainWindow::paint);
    toolsLayout->addWidget(paintButton);

    cropButton = new QPushButton;
    cropButton->setFixedSize(30, 30);
    cropButton->setIcon(QIcon(":/icons/crop"));
    connect(cropButton, &QPushButton::clicked, this, &MainWindow::crop);
    toolsLayout->addWidget(cropButton);

    filterButton = new QPushButton;
    filterButton->setFixedSize(30, 30);
    filterButton->setIcon(QIcon(":/icons/filter"));
    connect(filterButton, &QPushButton::clicked, this, &MainWindow::filter);
    toolsLayout->addWidget(filterButton);

    toolsLayout->addWidget(getVSpacer());

    mainLayout->addLayout(toolsLayout);


    editorDialogPane = new QWidget;
    QHBoxLayout *editorDialogLayout = new QHBoxLayout;
    editorDialogPane->setLayout(editorDialogLayout);

    QPushButton *okButton = new QPushButton;
    okButton->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    okButton->setText("OK");
    okButton->setMinimumSize(60, 30);
    okButton->setMaximumWidth(100);
    connect(okButton, &QPushButton::clicked, this, &MainWindow::editOK);
    editorDialogLayout->addWidget(okButton);

    QPushButton *cancelButton = new QPushButton;
    okButton->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    cancelButton->setText("Reset");
    cancelButton->setMinimumWidth(50);
    cancelButton->setMaximumWidth(80);
    cancelButton->setFixedHeight(25);
    connect(cancelButton, &QPushButton::clicked, this, &MainWindow::editCancel);
    editorDialogLayout->addWidget(cancelButton);

    mainLayout->addWidget(editorDialogPane);
    editorDialogPane->hide();

    editorHSVwidget = new QWidget;
    editorHSVwidget->setStyleSheet("*{border:0px;}");
    QHBoxLayout *editorHSVpane = new QHBoxLayout;
    colorWheel = new ColorWheel(QColor(255, 0, 0), 100, 100);
    editorHSVpane->addWidget(colorWheel);
    QVBoxLayout *colorSlidersPane = new QVBoxLayout;
    editorHSVpane->addLayout(colorSlidersPane);
    editorHSVwidget->setLayout(editorHSVpane);
    colorSlidersPane->addLayout(makeHSVslider(&hSlider, &hLabel, "H", &MainWindow::setH));
    colorSlidersPane->addLayout(makeHSVslider(&sSlider, &sLabel, "S", &MainWindow::setS));
    colorSlidersPane->addLayout(makeHSVslider(&vSlider, &vLabel, "V", &MainWindow::setV));
    mainLayout->addWidget(editorHSVwidget);
    editorHSVwidget->hide();

    editorPenWidthWidget = new QWidget;
    editorPenWidthWidget->setStyleSheet("*{border:0px;}");
    QHBoxLayout *penWidthLayout = new QHBoxLayout;
    editorPenWidthWidget->setLayout(penWidthLayout);
    penWidthSlider = new QSlider(Qt::Orientation::Horizontal);
    penWidthSlider->setRange(0, MAX_PEN_WIDTH);
    penWidthLayout->addWidget(new QLabel("Thickness"));
    penWidthLayout->addWidget(penWidthSlider);
    connect(penWidthSlider, &QSlider::sliderMoved, this, &MainWindow::setPenWidth);
    mainLayout->addWidget(editorPenWidthWidget);
    editorPenWidthWidget->hide();

    QWidget *mainSpacer = new QWidget;
    mainSpacer->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
    mainLayout->addWidget(mainSpacer);
}

QHBoxLayout *MainWindow::makeHSVslider(QSlider **slider, QLabel **label, QString name, void (MainWindow::*slot)(int)){
    *slider = new QSlider(Qt::Orientation::Horizontal);
    connect(*slider, &QSlider::sliderMoved, this, slot);
    QHBoxLayout *layout = new QHBoxLayout;
    QLabel *nameLbl = new QLabel(name);
    nameLbl->setFixedSize(10, 10);
    *label = new QLabel;
    (*label)->setFixedSize(30, 30);
    layout->addWidget(nameLbl);
    layout->addWidget(*slider);
    layout->addWidget(*label);
    return layout;
}

void MainWindow::initBottomPane(QVBoxLayout *mainLayout){
    QHBoxLayout *bottomPane = new QHBoxLayout;
    mainLayout->addLayout(bottomPane);

    infoLabel = new QLabel;
    infoLabel->setContentsMargins(5, 0, 0, 5);
    bottomPane->addWidget(infoLabel, 0, Qt::AlignCenter);

    bottomPane->addWidget(getVSpacer());

    pageLabel = new QLabel;
    pageLabel->setContentsMargins(0, 0, 5, 5);
    bottomPane->addWidget(pageLabel);
}

MainWindow::~MainWindow(){
    if(image != nullptr){
        delete image;
    }
    if(originalImg != nullptr){
        delete originalImg;
    }
}

void MainWindow::initGeometry(){
    const QRect available = QApplication::desktop()->availableGeometry(this);

    int windowW = int(available.width() * 0.7 + 0.5);
    int windowH = int(available.height() * 0.7 + 0.5);

    move((available.width() - windowW) / 2, (available.height() - windowH) / 2);
    resize(windowW, windowH);
    setMinimumSize(100, 100);
}

void MainWindow::createMenu(){
    setStyleSheet("QMenuBar{ background-color: #303030; font:bold; color:#ff0060}"
                  "QMenu{ background-color: #505050; font:bold}"
                  "QMenu:separator{height:2px; background:#606060}"
                  "QMenu:item{padding:2px 20px 2px 20px; border: 3px solid transparent}"
                  "QMenu:item:selected{border-color: #ff0060}");
    fileMenu = menuBar()->addMenu("File");

    fileAction = createAction("Open...", fileMenu, QKeySequence::Open, &MainWindow::open, "", true);
    saveAction = createAction("Save", fileMenu, QKeySequence::Save, &MainWindow::save);
    saveAsAction = createAction("Save as...", fileMenu, QKeySequence::SaveAs, &MainWindow::saveAs);

    viewMenu = menuBar()->addMenu("View");

    zoomInAction = createAction("Zoom In", viewMenu, QKeySequence::ZoomIn, &MainWindow::zoomIn, ":/icons/zoom_in");
    zoomOutAction = createAction("Zoom Out", viewMenu, QKeySequence::ZoomOut, &MainWindow::zoomOut, ":/icons/zoom_out");
    originalSizeAction = createAction("To original size", viewMenu, QKeySequence(Qt::CTRL + Qt::Key_Equal),
                                      &MainWindow::toOriginalSize, ":/icons/normal-size");
    fitToWindowAction = createAction("Fit to window", viewMenu, QKeySequence(Qt::CTRL + Qt::Key_W),
                                     &MainWindow::fitImageToWindow, ":/icons/fit");
    viewMenu->addSeparator();
    reloadAction = createAction("Reload", viewMenu, QKeySequence(Qt::Key_R), &MainWindow::reload);
    viewMenu->addSeparator();
    showToolbarAction = createAction("Show toolbar", viewMenu, QKeySequence(Qt::Key_T), &MainWindow::showToolbar);
    showToolbarAction->setCheckable(true);
    showToolbarAction->setChecked(true);

    goMenu = menuBar()->addMenu("Go");

    nextAction = createAction("Next image", goMenu, QKeySequence::Forward, &MainWindow::toNext, ":/icons/next");
    prevAction = createAction("Previous image", goMenu, QKeySequence::Back, &MainWindow::toPrevious, ":/icons/prev");
    randomAction = createAction("Random image",goMenu, QKeySequence(Qt::CTRL + Qt::Key_M), &MainWindow::toRandom);

    editMenu = menuBar()->addMenu("Edit");

    clockwiseAction = createAction("Rotate clockwise", editMenu, QKeySequence(Qt::CTRL + Qt::Key_R),
                                   &MainWindow::clockwise, ":/icons/rotate_clock");
    ctrclockwiseAction = createAction("Rotate counterclockwise", editMenu, QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_R),
                                      &MainWindow::ctrclockwise, ":/icons/rotate_ctr");
    editMenu->addSeparator();
    flipH_Action = createAction("Flip horisontally", editMenu, QKeySequence(Qt::CTRL + Qt::Key_F), &MainWindow::flipH,
                                ":/icons/flip_h");
    flipV_Action = createAction("Flip vertically", editMenu, QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_F), &MainWindow::flipV,
                                ":/icons/flip_v");
    editMenu->addSeparator();
    deleteAction = createAction("Delete", editMenu, QKeySequence(Qt::SHIFT + Qt::Key_Delete), &MainWindow::deleteImage);
    copyAcion = createAction("Copy", editMenu, QKeySequence(Qt::CTRL + Qt::Key_C), &MainWindow::copy);
    openEditorAction = createAction("Other...", editMenu, QKeySequence(Qt::CTRL + Qt::Key_E), &MainWindow::openEditor);
}

QAction *MainWindow::createAction(QString name, QMenu *menu, QKeySequence keySeq, void (MainWindow::*slot)(), QString icon, bool enabled){
    QAction *action = new QAction(name, this);
    if(icon != ""){
        action->setIcon(QIcon(icon));
    }
    menu->addAction(action);
    action->setShortcut(keySeq);
    connect(action, &QAction::triggered, this, slot);
    action->setEnabled(enabled);
    return action;
}

static void setActionsEnabled(QMenu *menu, bool enabled){
    for(auto action : menu->actions()){
        action->setEnabled(enabled);
    }
}

static inline QString imgFilter(){
    return "Imges (" + utils::supportedImageExtentions("*.").join(" ") + ")";
}

void MainWindow::open(){
    QString fileName = QFileDialog::getOpenFileName(this, "Select image file", explorerDir(), imgFilter());
    if(!fileName.isEmpty()){
        loadImage(fileName);
    }
}

bool MainWindow::loadImage(QString path){
    if(originalImg != nullptr){
        delete originalImg;
    }
    originalImg = new QImage;
    if(!originalImg->load(path)){
        return false;
    }

    setImage(*originalImg);

    setKernelImage(kernelCombo->currentText());

    setActionsEnabled(fileMenu, true);
    setActionsEnabled(viewMenu, true);
    setActionsEnabled(goMenu, true);
    setActionsEnabled(editMenu, true);
    setWindowTitle(utils::getFileName(path));

    fitImageToWindow();
    updateInfoLabel();

    loadDir(path);
    imagePath = path;
    int oldIndex = imageIndex;
    imageIndex = galeryFiles.indexOf(utils::getFileName(imagePath));
    if(oldIndex < galeryFiles.length() && oldIndex >= 0){
        moveSelection(imageIndex, oldIndex);
    }else{
        moveSelection(imageIndex);
    }

    pageLabel->setText(QString::number(imageIndex + 1) + "/" + QString::number(galeryFiles.length()));

    editCancel();
    return true;
}

void MainWindow::loadDir(QString filePath){
    QString fileDir = utils::getDirectoryName(filePath);
    QString curDir =  utils::getDirectoryName(imagePath);
    QStringList oldGaleryFiles = galeryFiles;
    galeryFiles = QDir(fileDir).entryList(utils::supportedImageExtentions("*."));
    if(galeryFiles.length() == 0){
        setActionsEnabled(viewMenu, false);
        setActionsEnabled(goMenu, false);
        setActionsEnabled(editMenu, false);
        imageBox->clear();
        setupGalery(fileDir);
        imagePath = "";
        return;
    }
    if(fileDir != curDir || oldGaleryFiles != galeryFiles){
        setupGalery(fileDir);
    }
}

void MainWindow::reload(){
    loadDir(imagePath);
    loadImage(imageIndex);
}

void MainWindow::showToolbar(){
    toolbar->setHidden(!toolbar->isHidden());
}

void MainWindow::save(){
    image->save(imagePath);
}

void MainWindow::saveAs(){
    QString fileName = QFileDialog::getSaveFileName(this, "Save as...(Export)", imagePath, imgFilter());
    QString ext = utils::getExtention(fileName);
    if(!utils::supportedImageExtentions().contains(ext)){
        QMessageBox mb;
        mb.setText("Dunno *." + ext + " extention man:( Savin' with the default");
        mb.exec();
    }
    image->save(fileName);
    reload();
}

QString MainWindow::explorerDir(){
    return imagePath == "" ? QDir::homePath() + "/Pictures/" : utils::getDirectoryName(imagePath);
}

void MainWindow::zoomIn(){
    toResizeMode();
    imageBox->zoom(getNextScale(true));
    updateInfoLabel();
}

void MainWindow::zoomOut(){
    toResizeMode();
    imageBox->zoom(getNextScale(false));
    updateInfoLabel();
}

double MainWindow::getNextScale(bool inc){
    double scale = imageBox->curScalePercent();

    //---------scale by 100% jumps
    if(inc){
        if(scale >= 20){    //upper scaling boundary
            return 20;
        }
        if(scale >= 1){
            return scale + 1;
        }
    }
    if(scale > 1 && !inc){
        if(scale <= 2){
            return 1;
        }
        return scale - 1;
    }

    //------------scale on scaleSeq

    int len = sizeof(scaleSeq) / 8;
    int offset = 0;
    int p = len / 2;
    double p1, p2;
    double prev, next;
    while(p != 0){
        int loc = p + offset;
        p1 = scaleSeq[loc - 1];
        p2 = scaleSeq[loc];
        prev = loc == 1? scale: scaleSeq[loc - 2];
        next = scaleSeq[loc + 1];

        if(scale > p1 && scale < p2){
            return inc? p2: p1;
        }
        if(int(scale * 100) == int(p1 * 100)){
            return inc? p2: prev;
        }
        if(int(scale * 100) == int(p2 * 100)){
            return inc? next: p1;
        }
        if(scale > p2){
            offset += p;
        }
        p /= 2;
    }
    return scale;
}

static QString thumbDefStyle = "border: 3px solid #606060;";

void MainWindow::setupGalery(QString directory){
    galeryLayout->clear();
    thumbs = *new QList<QPushButton *>();
    for(int i = 0; i < galeryFiles.length(); i++){
        QPushButton *thumb = new QPushButton;
        thumb->setStyleSheet(thumbDefStyle);
        QPixmap pixmap(directory + QDir::separator()+ galeryFiles.at(i));
        thumb->setIcon(QIcon(pixmap));
        thumb->setIconSize(QSize(100, int(pixmap.size().height() / double(pixmap.size().width()) * 100)));
        connect(thumb, &QPushButton::clicked, [this, i](){
            loadImage(i);
        });
        thumbs.append(thumb);
        galeryLayout->addWidget(thumb);
    }
}

void MainWindow::moveSelection(int index, int from){
    if(from != -1){
        thumbs[from]->setStyleSheet(thumbDefStyle);
    }
    thumbs[index]->setStyleSheet("border: 3px solid #ff0060;");
}

void MainWindow::toOriginalSize(){
    toResizeMode();
    imageBox->resize(imageBox->originalSize());
    updateInfoLabel();
}

void MainWindow::fitImageToWindow(){
    resizeMode = false;
    //cannot fit if scaled up so
    if(imageBox->curScalePercent() > 1){
        imageBox->resize(imageBox->originalSize());
    }
    imageScroll->setWidgetResizable(true);
    updateInfoLabel();
}

void MainWindow::toResizeMode(){
    resizeMode = true;
    imageScroll->setWidgetResizable(false);
    imageBox->fitToImage();
}

void MainWindow::updateInfoLabel(){
    char buf[100];

    if(resizeMode){
        snprintf(buf, sizeof(buf), "%ix%i %i%%",
                 imageBox->originalSize().width(),
                 imageBox->originalSize().height(),
                 int(imageBox->curScalePercent() * 100 + 0.5));
    }
    else{
        snprintf(buf, sizeof(buf), "%ix%i",
                 imageBox->originalSize().width(),
                 imageBox->originalSize().height());
    }
    infoLabel->setText(buf);
}

void MainWindow::toNext(){
    if(imageIndex != galeryFiles.length() - 1){
        loadImage(imageIndex + 1);
    }
}

void MainWindow::toPrevious(){
    if(imageIndex != 0){
        loadImage(imageIndex - 1);
    }
}

void MainWindow::toRandom(){
    loadImage(rand() % galeryFiles.length());
}

void MainWindow::loadImage(int index){
    if(index < galeryFiles.length() && index >= 0){
        loadImage(utils::getDirectoryName(imagePath) + QDir::separator() + galeryFiles.at(index));
    }
}

void MainWindow::setImage(const QImage &newImg){
    if(image != nullptr){
        delete image;
    }
    image = new QImage(newImg);
    imageBox->loadImage(ImgKernel::kernels.at(kernelCombo->currentText()).apply(*image));
}

void MainWindow::clockwise(){
    setImage(image->transformed(QMatrix().rotate(90)));
}

void MainWindow::ctrclockwise(){
    setImage(image->transformed(QMatrix().rotate(-90)));
}

void MainWindow::flipH(){
    setImage(image->mirrored(true, false));
}

void MainWindow::flipV(){
    setImage(image->mirrored(false, true));
}

void MainWindow::deleteImage(){
    QFile imageFile(imagePath);
    imageFile.remove();
    loadDir(imagePath);
    loadImage(imageIndex == galeryFiles.length()? imageIndex - 1: imageIndex);
}

void MainWindow::copy(){
    QClipboard *clipboard = QApplication::clipboard();
    if(QFile(imagePath).exists()){
        clipboard->setImage(QImage(imagePath));
    }
}

void MainWindow::openEditor(){
    editor->show();
}

void MainWindow::closeEditor(){
    editor->hide();
}

void MainWindow::crop(){
    closeEditTool();
    cropButton->setDisabled(true);
    editorDialogPane->show();
    imageBox->setEditMode(EditMode::Crop);
}

void MainWindow::paint(){
    closeEditTool();
    paintButton->setDisabled(true);
    editorDialogPane->show();
    editorHSVwidget->show();
    editorPenWidthWidget->show();
    penWidthSlider->setValue(imageBox->penWidth());
    imageBox->setEditMode(EditMode::Paint);
    hSlider->setRange(0, 360);
    sSlider->setRange(0, 255);
    vSlider->setRange(0, 255);
    QColor c = imageBox->penColor();
    setHSVsliders(c.hue(), c.saturation(), c.value());
}

void MainWindow::filter(){
    closeEditTool();
    filterButton->setDisabled(true);
    editorDialogPane->show();
    editorHSVwidget->show();
    imageBox->setEditMode(EditMode::Filter);
    hSlider->setRange(-180, 180);
    sSlider->setRange(-255, 255);
    vSlider->setRange(-255, 255);
    setHSVsliders(curFilter.dh, curFilter.ds, curFilter.dv);
}

void MainWindow::editOK(){
    closeEditTool();
    if(originalImg != nullptr){
        delete originalImg;
    }
    originalImg = new QImage(imageBox->apply()->toImage());
    setImage(*originalImg);
    imageBox->setEditMode(EditMode::None);
    resetFilter();
}

void MainWindow::editCancel(){
    if(imageBox->editMode() == EditMode::Filter){
        resetFilter();
    }
    imageBox->reset();
}

void MainWindow::resetFilter(){
    curFilter = ImgFilter(0, 0, 0);
    setImage(*originalImg);
    setHSVsliders(0, 0, 0);
}

void MainWindow::closeEditTool(){
    editorDialogPane->hide();
    editorHSVwidget->hide();
    editorPenWidthWidget->hide();
    paintButton->setEnabled(true);
    cropButton->setEnabled(true);
    filterButton->setEnabled(true);
}

void MainWindow::setHSVsliders(int h, int s, int v){
    hSlider->setValue(h);
    sSlider->setValue(s);
    vSlider->setValue(v);
    hLabel->setText(QString::number(h));
    sLabel->setText(QString::number(s));
    vLabel->setText(QString::number(v));
}

void MainWindow::setH(int val){
    setColorWheel(val, sSlider->value(), vSlider->value());
    hLabel->setText(QString::number(val));
    if(imageBox->editMode() == EditMode::Paint){
        setPenColor(val, sSlider->value(), vSlider->value());
    }else if(imageBox->editMode() == EditMode::Filter){
        setFilterImage(val, curFilter.ds, curFilter.dv);
    }
}

void MainWindow::setS(int val){
    setColorWheel(hSlider->value(), val, vSlider->value());
    sLabel->setText(QString::number(val));
    if(imageBox->editMode() == EditMode::Paint){
        setPenColor(hSlider->value(), val, vSlider->value());
    }else if(imageBox->editMode() == EditMode::Filter){
        setFilterImage(curFilter.dh, val, curFilter.dv);
    }
}

void MainWindow::setV(int val){
    setColorWheel(hSlider->value(), sSlider->value(), val);
    vLabel->setText(QString::number(val));
    if(imageBox->editMode() == EditMode::Paint){
        setPenColor(hSlider->value(), sSlider->value(), val);
    }else if(imageBox->editMode() == EditMode::Filter){
        setFilterImage(curFilter.dh, curFilter.ds, val);
    }
}

void MainWindow::setColorWheel(int h, int s, int v){
    colorWheel->setColor(QColor::fromHsv(h, s, v));
}

void MainWindow::setPenColor(int h, int s, int v){
    if(h > 359){
        h = 359;
    }
    imageBox->setPenColor(QColor::fromHsv(h, s, v));
}

void MainWindow::setFilterImage(int h, int s, int v){
    curFilter = ImgFilter(h, s, v);
    setImage(curFilter.apply(*originalImg));
}

void MainWindow::setKernelImage(QString){
    setFilterImage(curFilter.dh, curFilter.ds, curFilter.dv);
    setImage(*image);
}

void MainWindow::setPenWidth(int val){
    imageBox->setPenWidth(val);
}
